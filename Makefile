IMAGE         := flatpak:local
.DEFAULT_GOAL := help
SHELL          = /usr/bin/env bash -o pipefail
.SHELLFLAGS    = -ec

.PHONY: build
build: ## Build local container
	podman build --tag "$(IMAGE)" .

.PHONY: run
run: build ## Build and run local container
	podman run \
		--interactive \
		--tty \
		--rm \
		--privileged \
		--volume "${PWD}:/data:z" \
		--workdir /data \
		"$(IMAGE)"

.PHONY: help
help: ## Makefile Help Page
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[\/\%a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-21s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST) 2>/dev/null
