FROM docker.io/alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

# renovate: datasource=repology depName=alpine_3_21/bash versioning=loose
ARG BASH_VER=5.2.37-r0
# renovate: datasource=repology depName=alpine_3_21/curl versioning=loose
ARG CURL_VER=8.12.1-r0
# renovate: datasource=repology depName=alpine_3_21/flatpak versioning=loose
ARG FLATPAK_VER=1.14.10-r0
# renovate: datasource=repology depName=alpine_3_21/flatpak-builder versioning=loose
ARG FLATPAK_BUILDER_VER=1.4.4-r1

RUN apk add --no-cache \
        "bash=$BASH_VER" \
        "curl=$CURL_VER" \
        "flatpak=$FLATPAK_VER" \
        "flatpak-builder=$FLATPAK_BUILDER_VER" \
 && flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo
