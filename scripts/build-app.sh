#!/usr/bin/env bash
# Build a flatpak application and create a bundle
set -xeuo pipefail

BUILD_DIR=${CI_PROJECT_DIR}/build
REPO_DIR=${CI_PROJECT_DIR}/repo
BUNDLE_PATH=${CI_PROJECT_DIR}/${PACKAGE}.flatpak
BUNDLE_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE}/main/bundle.flatpak

# GPG key - import
gpg --import "$GPG_PRIVATE_KEY_PATH"
# Not sure why but sometimes it fails
set +e
gpg_public_key_id=$(
    gpg \
        --list-keys \
        --with-colons \
        "$GPG_EMAIL" |
        grep "fpr:" |
        head -1 |
        cut -d ':' -f 10
)
set -e

# Flatpak - Build
cd apps
if [ -d "$PACKAGE" ]; then
    cd "$PACKAGE"
fi
flatpak-builder \
    --gpg-sign "$gpg_public_key_id" \
    --install-deps-from flathub \
    --repo "$REPO_DIR" \
    --force-clean \
    "$BUILD_DIR" \
    "${PACKAGE}.yaml"

# Create bundle
cd "$CI_PROJECT_DIR"
flatpak build-bundle "$REPO_DIR" "$BUNDLE_PATH" "$PACKAGE"

# Upload bundle to Package Registry
curl \
    --header "JOB-TOKEN: $CI_JOB_TOKEN" \
    --upload-file "$BUNDLE_PATH" \
    "$BUNDLE_URL"
