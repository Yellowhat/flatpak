#!/usr/bin/env bash
# Collect flatpak application bundle
set -xeuo pipefail

PUBLIC_DIR=${CI_PROJECT_DIR}/public

# GPG key - import
gpg --import "$GPG_PRIVATE_KEY_PATH"
gpg_public_key_base64=$(base64 -w 0 <<<"$GPG_PUBLIC_KEY")
# Not sure why but sometimes it fails
set +e
gpg_public_key_id=$(
    gpg \
        --list-keys \
        --with-colons \
        "$GPG_EMAIL" |
        grep "fpr:" |
        head -1 |
        cut -d ':' -f 10
)
set -e

cd apps

# Use io.yellowhat.Hello app to initialize repository
flatpak-builder \
    --install-deps-from flathub \
    --repo "$PUBLIC_DIR" \
    --force-clean \
    build \
    io.yellowhat.Hello.yaml

find . -iname "io.*.yaml" -exec basename {} ".yaml" \; | while read -r pkg; do
    curl \
        --header "JOB-TOKEN: $CI_JOB_TOKEN" \
        --output "/tmp/${pkg}" \
        "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${pkg}/main/bundle.flatpak"
    flatpak build-import-bundle "$PUBLIC_DIR" "/tmp/${pkg}"
done

# Flatpak - Updates repository metadata
flatpak build-update-repo --gpg-sign "$gpg_public_key_id" "$PUBLIC_DIR"

# Flatpak - Create flatpakrepo file, required toa dd repo
cat >"${PUBLIC_DIR}/flathub.flatpakrepo" <<EOF
[Flatpak Repo]
Title=Yellowhat Flatpak Repository
Url=https://yellowhat-labs.gitlab.io/flatpak
Homepage=https://yellowhat-labs.gitlab.io/flatpak
Comment=Yellowhat Flatpak Repository
Description=Yellowhat Flatpak Repository
GPGKey=${gpg_public_key_base64}
EOF
