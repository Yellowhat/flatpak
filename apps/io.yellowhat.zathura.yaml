---
id: io.yellowhat.zathura

runtime: org.freedesktop.Platform
runtime-version: "24.08"
sdk: org.freedesktop.Sdk

command: zathura

finish-args:
  - --filesystem=home:ro
  - --filesystem=/media:ro
  - --filesystem=/run/media:ro
  - --filesystem=/mnt:ro
  # Required to show window and clipboard
  - --device=dri
  - --socket=wayland

cleanup:
  - /include
  - /lib/pkgconfig
  - '*.a'
  - '*.la'

modules:
  - name: zathura
    buildsystem: meson
    sources:
      - type: archive
        url: https://github.com/pwmt/zathura/archive/0.5.11.tar.gz
        sha256: 32540747a6fe3c4189ec9d5de46a455862c88e11e969adb5bc0ce8f9b25b52d4
        x-checker-data:
          type: anitya
          project-id: 5298
          stable-only: true
          url-template: https://github.com/pwmt/zathura/archive/$version.tar.gz
    post-install:
      - mkdir -p "${FLATPAK_DEST}/etc"
      - |
        cat <<EOF | tee "${FLATPAK_DEST}/etc/zathurarc"
        # Required to make clipboard work on wayland
        set selection-clipboard clipboard
        # Required to not show database warning
        set database sqlite
        EOF
    cleanup:
      - /share/bash-completion
      - /share/fish
      - /share/zsh
    modules:
      - name: girara
        buildsystem: meson
        sources:
          - type: archive
            url: https://github.com/pwmt/girara/archive/0.4.5.tar.gz
            sha256: 9abb84fdb3f8f51e8aef8d53488fd0631357f0713ad5aa4a5c755c23f54b23df
            x-checker-data:
              type: anitya
              project-id: 6750
              stable-only: true
              url-template: https://github.com/pwmt/girara/archive/$version.tar.gz
  - name: zathura-pdf-mupdf
    buildsystem: meson
    sources:
      - type: archive
        url: https://github.com/pwmt/zathura-pdf-mupdf/archive/0.4.4.tar.gz
        sha256: 90bdc7c0d4b5f6bd7b17f9c3832ae5eb8465b45d78ab3b8c2fca26ed45ed1177
        x-checker-data:
          type: anitya
          project-id: 14846
          stable-only: true
          url-template: https://github.com/pwmt/zathura-pdf-mupdf/archive/$version.tar.gz
      # https://github.com/pwmt/zathura-pdf-mupdf/issues/69
      - type: shell
        commands:
          - sed -i -e '/^mupdfthird =/d' -e 's/, mupdfthird//g' meson.build
    modules:
      - name: libmupdf
        no-autogen: true
        buildsystem: simple
        build-commands:
          # HAVE_X11=no: build only the command line tools, libraries and headers
          # HAVE_GLUT=no: do not build the OpenGL viewer
          # USE_SYSTEM_LIBS=yes: to avoid "Wrong JPEG library version: library is 62, caller expects 90"
          - make shared=yes build=release verbose=yes HAVE_X11=no HAVE_GLUT=no USE_SYSTEM_LIBS=yes prefix=${FLATPAK_DEST} install
          # Apparently zathura-pdf-mupdf cannot find libmupdf.so.x
          - install -m 644 build/shared-release/libmupdf.so.* "${FLATPAK_DEST}/lib/libmupdf.so"
        sources:
          - type: archive
            url: https://mupdf.com/downloads/archive/mupdf-1.25.4-source.tar.gz
            sha256: 74b943038fe81594bf7fc5621c60bca588b2847f0d46fb2e99652a21fa0d9491
            x-checker-data:
              type: anitya
              project-id: 2034
              stable-only: true
              url-template: https://mupdf.com/downloads/archive/mupdf-$version-source.tar.gz
        modules:
          - name: glu
            buildsystem: meson
            sources:
              - type: archive
                url: https://archive.mesa3d.org/glu/glu-9.0.3.tar.xz
                sha256: bd43fe12f374b1192eb15fe20e45ff456b9bc26ab57f0eee919f96ca0f8a330f
                x-checker-data:
                  type: anitya
                  project-id: 13518
                  stable-only: true
                  url-template: https://archive.mesa3d.org/glu/glu-$version.tar.xz
          # Required when USE_SYSTEM_LIBS=yes
          - name: jbig2dec
            sources:
              - type: archive
                url: https://github.com/ArtifexSoftware/jbig2dec/archive/0.20.tar.gz
                sha256: a9705369a6633aba532693450ec802c562397e1b824662de809ede92f67aff21
                x-checker-data:
                  type: anitya
                  project-id: 1431
                  stable-only: true
                  url-template: https://github.com/ArtifexSoftware/jbig2dec/archive/$version.tar.gz
            cleanup:
              - /bin
              - /share/man
          - name: gumbo-parser
            sources:
              - type: archive
                url: https://github.com/google/gumbo-parser/archive/v0.10.1.tar.gz
                sha256: 28463053d44a5dfbc4b77bcf49c8cee119338ffa636cc17fc3378421d714efad
                x-checker-data:
                  type: anitya
                  project-id: 214990
                  stable-only: true
                  url-template: https://github.com/google/gumbo-parser/archive/v$version.tar.gz
