#!/usr/bin/env bash

export TMPDIR=$XDG_CACHE_HOME/tmp

# Workaround for policies.json, due to:
#   - /app is read-only, therefore we cannot change policies.json after the flatpak is build
#   - "ln -sf ~/ ..." resolves to "ln -sf /root/ ..."
#   - could "ln -sf /home/foo/..." but it hardcodes the username
#   - /tmp is writable
# We can just copy .librewolf/policies.json to /tmp/policies.json on startup via librewolf.sh
POLICY_JSON="${HOME}/.librewolf/policies.json"
if [ -e "$POLICY_JSON" ]; then
    echo "$POLICY_JSON found"
    cp "$POLICY_JSON" "/tmp/policies.json"
fi

exec /app/lib/librewolf/librewolf "$@"
