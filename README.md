# flatpak

## How-to

```bash
flatpak --user remote-add --if-not-exists yellowhat https://yellowhat-labs.gitlab.io/flatpak/flathub.flatpakrepo
flatpak --user install -y yellowhat \
    io.yellowhat.nvim \
    io.yellowhat.Hello
flatpak run io.yellowhat.nvim io.yellowhat.Hello
```

### Add extensions

```bash
flatpak --user install -y flathub \
    flathub org.freedesktop.Sdk.Extension.rust-stable \
    flathub org.freedesktop.Sdk.Extension.node18
flatpak --user override --env=FLATPAK_ENABLE_SDK_EXT=* io.yellowhat.nvim
```

## GPG key

```bash
# Generate GPG Key
gpg \
    --batch \
    --passphrase '' \
    --quick-gen-key \
    test@dummy.com \
    future-default default 10y

# Export public key
gpg --export --armor test@dummy.com >public.key

# Export Private Key
gpg --export-secret-key --armor test@dummy.com >private.key
```

## pip

```bash
dnf install -y python-pip
pip install requirements-parser PyYAML
curl -LO https://raw.githubusercontent.com/flatpak/flatpak-builder-tools/master/pip/flatpak-pip-generator
python3 flatpak-pip-generator --yaml pynvim
python3 flatpak-pip-generator --yaml pulumi-aws
python3 flatpak-pip-generator --yaml pulumi-gcp
python3 flatpak-pip-generator --yaml pulumi-kubernetes
```

## x-checker-data

Use the [release-monitoring.org](https://release-monitoring.org)
website to get the project id
